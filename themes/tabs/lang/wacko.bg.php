<?php
$theme_translation = array(
	'EditIcon' => '<img src="'.$this->config['theme_url'].'icons/edit.gif" alt="Ðåäàêòèðàíå íà âëîæåíà ñòðàíèöà" border="0" />',
	'ACLStoreButton'  => 'Çàïèøè',
	'ACLCancelButton' => 'Îòêàæè \nè ñå âúðíè',
	'ACLAccessDenied' => '<em>Íå ñòå ñîáñòâåíèêà íà ñòðàíèöàòà.</em>',
	'EditStoreButton' => 'Çàïàçè ïðîìåíèòå',
	'EditCancelButton'  => 'Îòêàæè \nïðîìåíèòå',
	'EditPreviewButton' => 'Ïðåãëåä',

);
?>