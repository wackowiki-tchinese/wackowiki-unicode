<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Portuguese Version
* By Paulo Henrique Garcia - paulo@controllerweb.com.br
*/

$PHPMAILER_LANG['authenticate']         = 'Erro de SMTP: NÃ£o foi possÃ­vel autenticar.';
$PHPMAILER_LANG['connect_host']         = 'Erro de SMTP: NÃ£o foi possÃ­vel conectar com o servidor SMTP.';
$PHPMAILER_LANG['data_not_accepted']    = 'Erro de SMTP: Dados nÃ£o aceitos.';
//$PHPMAILER_LANG['empty_message']        = 'Message body empty';
$PHPMAILER_LANG['encoding']             = 'CodificaÃ§Ã£o desconhecida: ';
$PHPMAILER_LANG['execute']              = 'NÃ£o foi possÃ­vel executar: ';
$PHPMAILER_LANG['file_access']          = 'NÃ£o foi possÃ­vel acessar o arquivo: ';
$PHPMAILER_LANG['file_open']            = 'Erro de Arquivo: NÃ£o foi possÃ­vel abrir o arquivo: ';
$PHPMAILER_LANG['from_failed']          = 'Os endereÃ§os de rementente a seguir falharam: ';
$PHPMAILER_LANG['instantiate']          = 'NÃ£o foi possÃ­vel instanciar a funÃ§Ã£o mail.';
//$PHPMAILER_LANG['invalid_email']        = 'Not sending, email address is invalid: ';
$PHPMAILER_LANG['mailer_not_supported'] = ' mailer nÃ£o suportado.';
$PHPMAILER_LANG['provide_address']      = 'VocÃª deve fornecer pelo menos um endereÃ§o de destinatÃ¡rio de email.';
$PHPMAILER_LANG['recipients_failed']    = 'Erro de SMTP: Os endereÃ§os de destinatÃ¡rio a seguir falharam: ';
//$PHPMAILER_LANG['signing']              = 'Signing Error: ';
//$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP Connect() failed.';
//$PHPMAILER_LANG['smtp_error']           = 'SMTP server error: ';
//$PHPMAILER_LANG['variable_set']         = 'Cannot set or reset variable: ';
?>