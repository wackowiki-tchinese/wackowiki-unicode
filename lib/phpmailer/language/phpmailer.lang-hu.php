<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Hungarian Version
*/

$PHPMAILER_LANG['authenticate']         = 'SMTP Hiba: Sikertelen autentikÃ¡ciÃ³.';
$PHPMAILER_LANG['connect_host']         = 'SMTP Hiba: Nem tudtam csatlakozni az SMTP host-hoz.';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP Hiba: Nem elfogadhatÃ³ adat.';
//$PHPMAILER_LANG['empty_message']        = 'Message body empty';
$PHPMAILER_LANG['encoding']             = 'Ismeretlen kÃ³dolÃ¡s: ';
$PHPMAILER_LANG['execute']              = 'Nem tudtam vÃ©grehajtani: ';
$PHPMAILER_LANG['file_access']          = 'Nem sikerÃ¼lt elÃ©rni a kÃ¶vetkezÃµ fÃ¡jlt: ';
$PHPMAILER_LANG['file_open']            = 'FÃ¡jl Hiba: Nem sikerÃ¼lt megnyitni a kÃ¶vetkezÃµ fÃ¡jlt: ';
$PHPMAILER_LANG['from_failed']          = 'Az alÃ¡bbi FeladÃ³ cÃ­m hibÃ¡s: ';
$PHPMAILER_LANG['instantiate']          = 'Nem sikerÃ¼lt pÃ©ldÃ¡nyosÃ­tani a mail funkciÃ³t.';
//$PHPMAILER_LANG['invalid_email']        = 'Not sending, email address is invalid: ';
$PHPMAILER_LANG['provide_address']      = 'Meg kell adnod legalÃ¡bb egy cÃ­mzett email cÃ­met.';
$PHPMAILER_LANG['mailer_not_supported'] = ' levelezÃµ nem tÃ¡mogatott.';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP Hiba: Az alÃ¡bbi cÃ­mzettek hibÃ¡sak: ';
//$PHPMAILER_LANG['signing']              = 'Signing Error: ';
//$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP Connect() failed.';
//$PHPMAILER_LANG['smtp_error']           = 'SMTP server error: ';
//$PHPMAILER_LANG['variable_set']         = 'Cannot set or reset variable: ';
?>