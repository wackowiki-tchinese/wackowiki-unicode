<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Turkish version
* TÃ¼rkÃ§e Versiyonu
* ÃZYAZILIM - ElÃ§in Ãzel - Can YÃ½lmaz - Mehmet BenlioÃ°lu
*/

$PHPMAILER_LANG['authenticate']         = 'SMTP HatasÃ½: DoÃ°rulanamÃ½yor.';
$PHPMAILER_LANG['connect_host']         = 'SMTP HatasÃ½: SMTP hosta baÃ°lanÃ½lamÃ½yor.';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP HatasÃ½: Veri kabul edilmedi.';
//$PHPMAILER_LANG['empty_message']        = 'Message body empty';
$PHPMAILER_LANG['encoding']             = 'Bilinmeyen Ã¾ifreleme: ';
$PHPMAILER_LANG['execute']              = 'ÃalÃ½Ã¾tÃ½rÃ½lamÃ½yor: ';
$PHPMAILER_LANG['file_access']          = 'Dosyaya eriÃ¾ilemiyor: ';
$PHPMAILER_LANG['file_open']            = 'Dosya HatasÃ½: Dosya aÃ§Ã½lamÃ½yor: ';
$PHPMAILER_LANG['from_failed']          = 'BaÃ¾arÃ½sÃ½z olan gÃ¶nderici adresi: ';
$PHPMAILER_LANG['instantiate']          = 'Ãrnek mail fonksiyonu yaratÃ½lamadÃ½.';
//$PHPMAILER_LANG['invalid_email']        = 'Not sending, email address is invalid: ';
$PHPMAILER_LANG['provide_address']      = 'En az bir tane mail adresi belirtmek zorundasÃ½nÃ½z alÃ½cÃ½nÃ½n email adresi.';
$PHPMAILER_LANG['mailer_not_supported'] = ' mailler desteklenmemektedir.';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP HatasÃ½: alÃ½cÃ½lara ulaÃ¾madÃ½: ';
//$PHPMAILER_LANG['signing']              = 'Signing Error: ';
//$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP Connect() failed.';
//$PHPMAILER_LANG['smtp_error']           = 'SMTP server error: ';
//$PHPMAILER_LANG['variable_set']         = 'Cannot set or reset variable: ';
?>